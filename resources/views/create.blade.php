<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

        
    </head>
    <body>

        <div class="container">
<h3>Create task</h3>


<div class="row">
<div class="col-md-12">

<div class="form-group">
<input type="text" class="form-control" name="title">
<br>
<textarea name="description" id="" cols="30" rows="10" class="form-control">
</textarea>
<br>
<button class="btn btn-success">Submit</button>
</div>
</form>
</div>
</div>
</div> 

</body>
</html>
