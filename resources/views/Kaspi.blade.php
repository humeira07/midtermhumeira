<!DOCTYPE html>
<html lang="en">
<div class="wrapper">
<head>
<link rel="shortcut icon" href="img/logo1.png" />
  <title>Каспи Магазин в Алматы - интернет-магазин электроники и бытовой техники в кредит</title>
  <link rel="stylesheet" href="css.css">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<div id="wrapper"> 
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#"></a>
	   <img alt="Brand" src="img/logo.png"/>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Магазин</a></li>
      <li><a href="https://kaspi.kz/pay/how-to-pay">Платежи</a></li>
      <li><a href="https://kaspi.kz/bank/entrance?ReturnUrl=%2fbank%2fdashboard%2f">Мой Банк</a></li>
      <li><a href="https://kaspi.kz/red/">Red</a></li>
	   <li><a href="https://kaspi.kz/bonus/entrance">Бонус</a></li>
	    <li><a href="https://kaspi.kz/guide">Гид</a></li>
		 <li><a href="https://kaspi.kz/maps/terminals/">Maps</a></li>
		  <li><a href="https://kaspi.kz/transfers/landing/">Переводы</a></li>
    </ul>

 <ul class="nav navbar-nav navbar-right">
      <li><a href="https://kaspi.kz/entrance"><span class="glyphicon glyphicon-user"></span></a></li>
 </ul>
 </div>
 </nav>
 
 <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
          
                <div class="input-group stylish-input-group">
                    <input type="text" class="form-control"  placeholder="Search" >
                    <span class="input-group-addon">
                        <button type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>  
                    </span>
                </div>
            </div>
        </div>
	
 
 <nav class="navbar navbar-default">
  <div class="container__el">
    
    <ul class="nav navbar-nav">
    
      <li><a href="https://kaspi.kz/shop/search/?q=%3AdiscountOldPrice%3Aда">СКИДКИ</a></li>
      <li><a href="https://kaspi.kz/shop/c/smartphones%20and%20gadgets/">ТЕЛЕФОНЫ И ГАДЖЕТЫ</a></li>
      <li><a href="https://kaspi.kz/shop/c/tv_audio/">ТВ И АУДИО</a></li>
	  <li><a href="https://kaspi.kz/shop/c/computers/">КОМПЬЮТЕРЫ</a></li>
	  <li><a href="https://kaspi.kz/shop/c/home%20equipment/">ТЕХНИКА ДЛЯ ДОМА</a></li>
	  <li><a href="https://kaspi.kz/shop/c/kitchen%20equipment/">ТЕХНИКА ДЛЯ КУХНИ</a></li>
	  <li><a href="https://kaspi.kz/shop/c/car%20goods/">АВТО ТОВАРЫ</a></li>
	   <li><a href="https://kaspi.kz/shop/c/child%20goods/">ДЕТСКИЕ
ТОВАРЫ</a></li>
	    
	   
    </ul>
  </div>
</nav>
 
 
 
 
 
<div class="container">

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="img/iphone.jpg" alt="Los Angeles" style="width:50%;">
      </div>

      <div class="item">
        <img src="img/iphone2.jpg" alt="Chicago" style="width:50%;">
		
      </div>
    
      
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>


<main>
	
<div class="row">
 
    <div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/1.png" alt="Nature" >
          <div class="caption">
		  

        <a href='Haier'>      <p>Haier LE32B8500T Black-Silver</p></a>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(89 отзывов)</p></a>
			
			 <div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">54 900 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">2 930 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
        
		
          </div>
        </a>
      </div>
    </div>
	 <div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/2.png" alt="Nature" >
          <div class="caption">
           <a> <p>LG V30+ LGH930DS Moroccan Blue</p></a>
			<span class="glyphicon glyphicon-star-empty"></span>
			<span class="glyphicon glyphicon-star-empty"></span>
			<span class="glyphicon glyphicon-star-empty"></span>
			<span class="glyphicon glyphicon-star-empty"></span>
			<span class="glyphicon glyphicon-star-empty"></span>
			 <div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">71 990 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">3 842 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
          </div>
        </a>
      </div>
    </div>
	
    <div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/3.png" alt="Fjords" >
          <div class="caption">
           <a> <p>LED Haier LE32K5500T Black</p></a>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(365 отзывов)</p></a>
			<div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">235 000 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">12 542 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
          </div>
        </a>
      </div>
    </div>
	
	<div class="col-md-3">
      <div class="thumbnail">
          <img src="img/4.png" alt="Fjords" >
          <div class="caption">
          <a href='deal'>  <p>OnePlus 5T 128Gb Black</p></a>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(6 отзывов)</p></a>
			<div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">149 990 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">8 005 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>



<h1>Популярные товары</h1>
<div class="row">
 
    <div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/5.jpg" alt="Nature" >
          <div class="caption">
            <p>Apple iPhone 8 64Gb Space Gray</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(188 отзывов)</p></a>
			
			 <div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">289 800 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">15 466 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
        
		
          </div>
        </a>
      </div>
    </div>
	 <div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/6.jpg" alt="Nature" >
          <div class="caption">
            <p>Samsung Galaxy J5 SM-J510FN/DS Black</p>
			<span class="glyphicon glyphicon-star-empty"></span>
			<span class="glyphicon glyphicon-star-empty"></span>
			<span class="glyphicon glyphicon-star-empty"></span>
			<span class="glyphicon glyphicon-star-empty"></span>
			<span class="glyphicon glyphicon-star-empty"></span>
			<a><p>(1 606 отзывов)</p></a>	
			 <div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">50 115 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">2 675 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
          </div>
        </a>
      </div>
    </div>
	
     <div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/1.png" alt="Nature" >
          <div class="caption">
            <p>Haier LE32B8500T Black-Silver</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(89 отзывов)</p></a>
			
			 <div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">54 900 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">2 930 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
        
		
          </div>
        </a>
      </div>
    </div>
	
	
	<div class="col-md-3">
      <div class="thumbnail">
          <img src="img/7.jpg" alt="Fjords" >
          <div class="caption">
            <p>Samsung Galaxy J5 SM-J510FN/DS Gold</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(2 475 отзывов)</p></a>
			<div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">50 140 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">2 676 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
          </div>
        
      </div>
    </div>

	<div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/8.jpg" alt="Nature" >
          <div class="caption">
            <p>Xiaomi Redmi 4X 32Gb Black</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(724 отзывов)</p></a>
			
			 <div class="item-card__prices">
                            <div class="item-card__debet">
                                <span class="item-card__prices-title">Цена</span>
                                <span class="item-card__prices-price">48 880 ₸</span>
                                </div>
                            <div class="item-card__instalment">
                                <span class="item-card__prices-title">В кредит</span>
                                <span class="item-card__prices-price">2 609 ₸</span>
                                <span class="item-card__add-info">x 24 мес</span>
                            </div>
                        </div>
        
		
          </div>
        </a>
      </div>
    </div>

<div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/9.jpg" alt="Nature" >
          <div class="caption">
            <p>Samsung WF60F1R2F2W White</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(204 отзывов)</p></a>
			
			<div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">94 590 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">5 048 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
        
		
          </div>
        </a>
      </div>
    </div>

	<div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/10.jpg" alt="Nature" >
          <div class="caption">
            <p>Polaris PHB 0510A Silver-Black</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(81 отзывов)</p></a>
			<div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">10 590 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">1 896 ₸</span>
                    <span class="item-card__add-info">
                        x 6 мес</span>
                </div>
            </div>
			
        
		
          </div>
       
      </div>
    </div>
<div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/11.jpg" alt="Nature" >
          <div class="caption">
            <p>Elenberg TAB730 Black</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(47 отзывов)</p></a>
					
         <div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">18 220 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">3 262 ₸</span>
                    <span class="item-card__add-info">
                        x 6 мес</span>
                </div>
            </div>
		
          </div>
       
      </div>
    </div>	

<div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/12.jpg" alt="Nature" >
          <div class="caption">
            <p>Elenberg MG-2011M White</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(134 отзывов)</p></a>
					
         <div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">17 360 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">3 108 ₸</span>
                    <span class="item-card__add-info">
                        x 6 мес</span>
                </div>
            </div>
		
          </div>
       
      </div>
    </div>	
	
	<div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/13.png" alt="Nature" >
          <div class="caption">
            <p>Lenovo IdeaPad Yoga 300-11IBR 80M100RARK Black-White</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(11 отзывов)</p></a>
					
          <div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">89 990 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">4 803 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
		
          </div>
       
      </div>
    </div>	
	
	<div class="col-md-3">
      <div class="thumbnail">
       
          <img src="img/14.jpg" alt="Nature" >
          <div class="caption">
            <p>StarLine A93 Eco</p>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<span class="glyphicon glyphicon-star"></span>
			<a><p>(84 отзывов)</p></a>
					
       <div class="item-card__prices">                                                                                                                     
                <div class="item-card__debet">
                    <span class="item-card__prices-title">Цена</span>
                    <span class="item-card__prices-price">44 700 ₸</span>
                    </div>
                <div class="item-card__instalment">
                	<span class="item-card__prices-title">В кредит</span>
                    <span class="item-card__prices-price">2 386 ₸</span>
                    <span class="item-card__add-info">
                        x 24 мес</span>
                </div>
            </div>
		
          </div>
       
      </div>
    </div>	


	
</div>
</div>
</main>

<footer>
<div class="footer">  
		<div class="footer__section footer__section_login">
			<h4 class="footer__section-title">Покупателям</h4>
			<ul class="footer__section-list">
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/how_to_buy/"  title="Выбор товара" >Выбор товара</a></li>
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/about_credit/"  title="Покупка товара в кредит" >Покупка товара в кредит</a></li>
			</ul>
			<h4 class="footer__section-title">Мой kaspi.kz</h4>
			<ul class="footer__section-list">
			<li class="footer__section-list-item">
					<a data-seo-hide-link="https://kaspi.kz/shop/login/checkout" href="#">Вход/Регистрация</a>
				</li>
			</ul>
		</div>
		<div class="footer__section footer__section_catalog">
			<h4 class="footer__section-title">Каталог товаров</h4>
			<ul class="footer__section-list">
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/c/smartphones%20and%20gadgets/?ref=footer"    >Смартфоны и гаджеты</a>
				</li>
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/c/computers/?ref=footer"    >Компьютеры</a>
				</li>
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/c/home%20equipment/?ref=footer"    >Техника для дома</a>
				</li>
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/c/kitchen%20equipment/?ref=footer"    >Техника для кухни</a>
				</li>
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/c/tv_audio/?ref=footer"    >ТВ и Аудио</a>
				</li>
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/c/car%20goods/?ref=footer"    >Авто товары</a>
				</li>
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/c/child%20goods/?ref=footer"    >Детские товары</a>
				</li>
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/c/leisure/?ref=footer"    >Досуг</a>
				</li>
			<li class="footer__section-list-item">
					<a href="https://kaspi.kz/shop/c/photo_video/?ref=footer"    >Фото и Видео</a>
				</li>
			</ul>
		</div>
		
			<div class="footer__section footer__section_customers">
			<h4 class="footer__section-title">Партнерам</h4>
			<ul class="footer__section-list">
			<li class="footer__section-list-item">
						<a class="footer__section-list-item-link" data-seo-hide-link="https://kaspi.kz/merchantcabinet/" href="#"  >Вход в кабинет продавца</a>
					</li>
				</ul>
			<h4 class="footer__section-title _documents">Документы</h4>
			<ul class="footer__section-list">
			<li class="footer__section-list-item">	
					<a	href="https://kaspi.kz/shop/legal/user_agreement.pdf"  >  Пользовательское соглашение</a>
				</li>
			<li class="footer__section-list-item">	
					<a	href="https://kaspi.kz/shop/legal/bank_services_offer.pdf"  >  Договор присоединения</a>
				</li>
			</ul>
		</div>


		<div itemscope itemtype="http://schema.org/Organization">
			<div class="footer__section footer__section_contacts">
				<h4 class="footer__section-title">Помощь и контакты</h4>
				<div class="footer__section-telephone">
					<div class="phone ">
								<span class="phone__icon icon _small _mobile-toggle"></span>
								<span class="phone__number" itemprop="telephone">5555</span>
								<span class="phone__hint">(бесплатно)</span>
							</div>
						</div>
				<a href="mailto:help_shop@kaspi.kz" class="footer__section-mail">
					<span itemprop="email">help_shop@kaspi.kz</span>
				</a>
				<!---
				<div class="footer__section-socials">
				  <a class="btn btn-block btn-social btn-twitter">
   			<a class="footer__section-socials-link" href=https://www.facebook.com/kaspibank><span class="icon _medium _facebook"></span></a>
					<a class="footer__section-socials-link" href=https://vk.com/kaspibank><span class="icon _medium _vk"></span></a>
					<a class="footer__section-socials-link" href=https://twitter.com/kaspibank><span class="icon _medium _twitter"></span></a>
					<a class="footer__section-socials-link" href=http://instagram.com/kaspibank><span class="icon _medium _instagram"></span></a>
					<a class="footer__section-socials-link" href=http://my.mail.ru/community/kaspibank><span class="icon _medium _mailru"></span></a>
					<a class="footer__section-socials-link" href=http://www.odnoklassniki.ru/kaspibank/><span class="icon _medium _odnoklassniki"></span></a>
				</div>
			</div>
--->

<a href="https://www.facebook.com/kaspibank" class="fa fa-facebook"></a>
<a href="https://twitter.com/kaspibank" class="fa fa-twitter"></a>


			<div class="footer__copyright">
				<span>&copy; ТОО «Kaspi Магазин», 2015-2018</span>
			</div>

		</div>

    </div>
</div>




</footer>


</body>

</div>
</html>
