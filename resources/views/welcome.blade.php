<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

        
    </head>
    <body>

        <div class="container">
<h3>My list</h3>
<a href='create' class="btn btn-succsess">Create</a>
<div class="row">
<div class="col-md-10 col-md-offset-1">
<table class="table">
<thead>
<tr>
<td>ID</td> 
<td>Title</td>
<td>
<a href="#">
<i class="glyphicon glyphicon-eye-open"></i>
</a>
<a href="#">
<i class="glyphicon glyphicon-edit"></i>
</a>
<a href="#">
<i class="glyphicon glyphicon-remove"></i>
</a>
<td>Actions</td>
</tr>
</thead>
<tbody>
<tr>
<td>1</td> 
<td>Title..</td>
<td>Shows | Edit | Delete </td>
<tr>
</tbody>

</table>
</div>
</div>
</div> 

</body>
</html>
